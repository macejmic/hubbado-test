require_relative 'prime_numbers_generator'
require_relative 'fibonacci_numbers_generator'
require_relative 'main'

puts 'Define X size of a result.'
x_size = gets.chomp.to_i
puts "Your X size is: #{x_size}"

if x_size <= 0
  puts "X size #{x_size} should be bigger then a zero."
  exit(false)
end

puts 'Define Y size of a result.'
y_size = gets.chomp.to_i
puts "Your Y size is: #{y_size}"

if y_size <= 0
  puts "Y size #{y_size} should be bigger then a zero."
  exit(false)
end

generators = {
  1 => PrimeNumbersGenerator,
  2 => FibonacciNumbersGenerator
}

puts "Supported generators:"
generators.each do |generator|
  puts "#{generator[0]} - #{generator[1].to_s}"
end

generator_choices = generators.keys

puts "Choose a generator: #{generator_choices.join("/")}"
# to_i is also a code injection protection, don't remove it
choosen_generator = gets.chomp.to_i

if !generator_choices.include?(choosen_generator)
  puts "Invalid generator #{choosen_generator}. Start again."
  exit(false)
end

choosen_generator = generators[choosen_generator]

puts "Choosen generator: #{choosen_generator.to_s}"

# TODO - catch errors and print user friendly message
puts Main.run(x_size: x_size, y_size: y_size, generator: choosen_generator)

exit(true)