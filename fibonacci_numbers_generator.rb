require_relative 'base_numbers_generator'

class FibonacciNumbersGenerator < BaseNumbersGenerator
  attr_reader :fibonacci

  def initialize
    @fibonacci = Hash.new do |hsh, i| 
      hsh[i] = fibonacci[i - 2] + fibonacci[i - 1]
    end.update(0 => 0, 1 => 1)

    # count & cache first 1000 fibonacci numbers out of the box
    fibonacci[1000]
  end

  def self.take(first: 0)
    first_n = first.to_i
    return [] if (first_n <= 0)
    instance.fibonacci[first_n]
    instance.fibonacci.first(first_n).to_h.values
  end
end
