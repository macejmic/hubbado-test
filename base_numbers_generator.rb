require 'singleton'

class BaseNumbersGenerator
  include Singleton

  def initialize
  end

  def self.take(first: 0)
    raise NotImplementedError, 'the force cannot be used without the implementation'
  end
end