require_relative 'array_row_calculator'

class Main

  def self.run(x_size:, y_size:, generator:)
    numbers = generator.take(first: (y_size >= x_size) ? y_size : x_size)

    y_size.times.map do |index|
      row_array = ArrayRowCalculator.new(numbers.slice(0, x_size), numbers[index]).calculate
      row_array.join ' '
    end
  end
end