### Run

`$ ruby run.rb`


### Test

1. `$ gem install minitest`
2. `$ ruby -Itest test/all.rb`


### Dev notes

Want to add a new numbers generator?

1. follow `base_numbers_generator.rb` interface
2. add a new geneator to a generator lists in `run.rb`