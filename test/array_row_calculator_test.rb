require 'minitest/autorun'
require_relative '../array_row_calculator'

class ArrayRowCalculatorTest < Minitest::Test

  CALCULATOR = ArrayRowCalculator

  def test_calculate_no_numbers
    assert_equal [], CALCULATOR.new([], 0).calculate
  end

  def test_calculate_zero
    assert_equal [0], CALCULATOR.new([0], 0).calculate
  end

  def test_calculcate_number_with_number
    assert_equal [123, 246], CALCULATOR.new([1, 2], '123-and-text').calculate
  end

  def test_calculcate_not_valid_number
    assert_equal [0], CALCULATOR.new([0], 'not-valid-number').calculate
  end

  def test_calculcate_not_valid_numbers
    assert_equal [1, 0], CALCULATOR.new([1, 'not-valid-number'], 1).calculate
  end

  def test_calculate_real_case
    assert_equal [2, 4, 6], CALCULATOR.new([1, 2, 3], 2).calculate
  end
end