require 'minitest/autorun'
require_relative '../main'
require_relative '../base_numbers_generator'

class DummieGenerator < BaseNumbersGenerator
  def self.take(first:)
    [0, 1, 2, 3].first(first)
  end
end

class TestMain < Minitest::Test

  def test_main
    result = Main.run(x_size: 3, y_size: 5, generator: DummieGenerator)

    assert_equal 5, result.length
    assert_equal '0 0 0', result[0]
    assert_equal '0 1 2', result[1]
    assert_equal '0 2 4', result[2]
  end
end