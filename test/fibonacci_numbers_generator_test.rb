require 'minitest/autorun'
require_relative '../fibonacci_numbers_generator'

class FibonacciNumbersGeneratorTest < Minitest::Test

  GENERATOR = FibonacciNumbersGenerator

  def test_take_not_valid_argument
    assert_equal [], GENERATOR.take(first: 'not-valid-take-argument')
  end

  def test_take_nil
    assert_equal [], GENERATOR.take(first: nil)
  end

  def test_take_zero
    assert_equal [], GENERATOR.take(first: 0)
  end

  def test_take_negative_number
    assert_equal [], GENERATOR.take(first: -2)
  end

  def test_take_number_with_text
    assert_equal 10, GENERATOR.take(first: '10-and-text').length
  end

  def test_take_real_case
    assert_equal [0, 1, 1, 2, 3, 5, 8, 13, 21, 34], GENERATOR.take(first: 10)
  end
end