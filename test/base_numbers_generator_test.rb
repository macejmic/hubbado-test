require 'minitest/autorun'
require_relative '../base_numbers_generator'

class BaseNumbersGeneratorTest < Minitest::Test

  GENERATOR = BaseNumbersGenerator

  def test_take
    assert_raises NotImplementedError do
      GENERATOR.take(first: 5)
    end
  end
end