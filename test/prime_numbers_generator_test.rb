require 'minitest/autorun'
require_relative '../prime_numbers_generator'

class PrimeNumbersGeneratorTest < Minitest::Test

  GENERATOR = PrimeNumbersGenerator

  def test_take_not_valid_argument
    assert_equal [], GENERATOR.take(first: 'not-valid-take-argument')
  end

  def test_take_nil
    assert_equal [], GENERATOR.take(first: nil)
  end

  def test_take_zero
    assert_equal [], GENERATOR.take(first: 0)
  end

  def test_take_negative_number
    assert_equal [], GENERATOR.take(first: -2)
  end

  def test_take_number_with_text
    assert_equal 20, GENERATOR.take(first: '20-and-text').length
  end

  def test_take_real_case
    assert_equal [2, 3, 5, 7, 11, 13, 17, 19, 23, 29], GENERATOR.take(first: 10)
  end

  def test_hardcoded_vs_generated
    hardcoded = GENERATOR.take(first: 100)
    generated = GENERATOR.take(first: 1200).slice(0, 100)

    assert_equal hardcoded, generated
  end

  def test_4000
    assert_equal 40000, GENERATOR.take(first: 40000).length
  end

  def test_40001
    assert_raises ArgumentError do
      GENERATOR.take(first: 40001)
    end
  end
end