class ArrayRowCalculator
  attr_reader :array, :number

  def initialize(array, number)
    @array = Array(array)
    @number = number.to_i
  end

  def calculate
    array.map { |array_number| array_number.to_i * number }
  end
end